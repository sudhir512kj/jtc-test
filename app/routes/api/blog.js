const express = require("express");
const router = express.Router();
const passport = require("passport");
const errorHandler = require("./../../helpers/dbErrorHandler");
const keys = require("../../config/keys");
const expressJwt = require("express-jwt");
const formidable = require("formidable");
const jwtDecode = require("jwt-decode");
const fs = require("fs");

// Load input validation
const validatePostInput = require("../../validation/post");

// Load Blog model
const Blog = require("../../models/Blog");

const requireSignin = expressJwt({
  secret: keys.secretOrKey,
  userProperty: "auth",
  session: true
});

// @route POST api/blog/posts/new
// @desc Create post
// @access Private
router.post("/posts/new", requireSignin, (req, res, next) => {
  const { errors, isValid } = validatePostInput(req.body);
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const newPost = new Blog({
    title: req.body.title,
    content: req.body.content,
    postedBy: req.auth
  });
  newPost
    .save()
    .then(result => res.json(result))
    .catch(err => console.log(err));
});

// @route GET api/blog/posts
// @desc Get all posts
// @access Public
router.get("/posts", (req, res, next) => {
  Blog.find().exec((err, posts) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    res.json(posts);
  });
});

// @route GET api/blog/posts/user
// @desc Get all posts
// @access Public
router.get("/posts/user", requireSignin, (req, res, next) => {
  Blog.find({ postedBy: req.auth }).exec((err, posts) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    res.json(posts);
  });
});

// @route GET api/blog/posts/new
// @desc Get a post
// @access Public
router.get("/posts/:postId", (req, res, next) => {
  Blog.findById(postId)
    .populate("postedBy", "_id name")
    .exec((err, post) => {
      if (err || !post)
        return res.status("400").json({
          error: "Post not found"
        });
      req.post = post;
      next();
    });
});

// @route PUT api/blog/posts/like
// @desc Like Post
// @access Private
router.put("/posts/like", requireSignin, (req, res, next) => {
  Blog.findByIdAndUpdate(
    req.body.postId,
    { $push: { likes: req.auth } },
    { new: true }
  ).exec((err, result) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    res.json(result);
  });
});

// @route PUT api/blog/posts/unlike
// @desc Unlike Post
// @access Private
router.put("/posts/unlike", requireSignin, (req, res, next) => {
  Blog.findByIdAndUpdate(
    req.body.postId,
    { $pull: { likes: { id: req.auth.id } } },
    { new: true }
  ).exec((err, result) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    res.json(result);
  });
});

// @route PUT api/blog/posts/comment
// @desc Comment Post
// @access Private
router.put("/posts/comment", requireSignin, (req, res, next) => {
  let comment = { text: req.body.comment };
  comment.postedBy = req.auth;
  Blog.findByIdAndUpdate(
    req.body.postId,
    { $push: { comments: comment } },
    { new: true }
  ).exec((err, result) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    res.json(result);
  });
});

// @route PUT api/blog/posts/uncomment
// @desc Uncomment Post
// @access Private
router.put("/posts/uncomment", requireSignin, (req, res, next) => {
  let comment = req.body.comment;
  Blog.findByIdAndUpdate(
    req.body.postId,
    { $pull: { comments: { _id: comment._id } } },
    { new: true }
  ).exec((err, result) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      });
    }
    res.json(result);
  });
});

module.exports = router;
