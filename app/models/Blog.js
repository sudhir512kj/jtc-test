const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const BlogSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  photo: {
    data: Buffer,
    contentType: String
  },
  postedBy: {
    type: Object,
    required: true
  },
  likes: [
    {
      type: Object
    }
  ],
  comments: [
    {
      text: { type: String, required: true },
      created: { type: Date, default: Date.now },
      postedBy: { type: Object }
    }
  ],
  created: {
    type: Date,
    default: Date.now
  }
});

module.exports = User = mongoose.model("blog", BlogSchema);
