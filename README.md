# JTC Test

## Description

It is a MERN Stack blog for JTC Test.

## How to run

Need docker and docker-compose to run

Steps-

<ol>
    <li>
    Clone the repo and change directory to project
    <code>cd jtc-test</code>
    </li>
    <li>
    Build docker image
    <code>docker-compose build</code>
    </li>
    <li>
    Run docker image
    <code>docker-compose up -d</code>
    </li>
    <li>
    Check logs
    <code>docker-compose logs -f</code>
    </li>
</ol>
